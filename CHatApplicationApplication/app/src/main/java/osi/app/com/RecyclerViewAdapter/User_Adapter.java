package osi.app.com.RecyclerViewAdapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import osi.app.com.Model.UserList;
import osi.app.com.chatapplicationapplication.R;
import osi.app.com.chatapplicationapplication.UserListActivity;

/**
 * Created by Ashish PAtil on 2015/01/03.
 */
public class User_Adapter extends
        RecyclerView.Adapter<User_Adapter.ViewHolder> {

    private UserListActivity mContext;
    ArrayList<UserList> mDataSet;
    String Messsage = "Added to Wishlist";

    public User_Adapter(UserListActivity context,
                        ArrayList<UserList> dataSet) {
        mContext = context;
        mDataSet = dataSet;

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(
                R.layout.userlist_containr, parent, false);

        v.setId(viewType);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserList Data = mDataSet.get(position);
        holder.title.setText(Data.username.trim());


        holder.itemView.setId(position);

        holder.itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(mDataSet.get(view.getId()).username);
                Toast.makeText(mContext, "Copied", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void remove(int position) {
        mDataSet.remove(position);
        // notifyDataSetChanged();
        notifyItemRemoved(position);
    }

    // public void removeall() {
    // mCity_Select.CityClass_Data.removeAll(mCity_Select.CityClass_Data);
    // for (int i = 0; i < mCity_Select.CityClass_Data.size(); i++) {
    // mCity_Select.CityClass_Data.remove(i);
    // // notifyDataSetChanged();
    // notifyItemRemoved(i);
    //
    // }
    //
    // }

    public void add(UserList text, int position) {

        mDataSet.add(position, text);
        notifyItemInserted(position);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        LinearLayout tap_to_copy;
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = (TextView) itemView.findViewById(R.id.title);


        }
    }


}
